# Use Python 3.11-slim as the base image
FROM python:3.11-slim

# Set the working directory in the container
WORKDIR /app

# Copy your FastAPI code into the container
COPY main.py /app/

# Install the required Python packages
RUN pip install fastapi uvicorn 
RUN pip install -U pip setuptools wheel
RUN pip install -U spacy

# Copy your model folder into the container
COPY ./base_model /app/model

# Expose port 80 for FastAPI
EXPOSE 80

# Command to run the FastAPI server using uvicorn
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]