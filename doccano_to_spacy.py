import sys
import json


def convert_doccano_to_spacy(doccano_JSON_FilePath):
    try:
        training_data = []
        lines=[]
        with open(doccano_JSON_FilePath, 'r') as f:
            lines = f.readlines()

        for line in lines:
            data = json.loads(line)
            text = data['text']
            entities = data['label']
            if len(entities)>0:
                training_data.append((text, {"entities" : entities}))
        return training_data
    except Exception as e:
        logging.exception("Unable to process " + dataturks_JSON_FilePath + "\n" + "error = " + str(e))
        return None


training_data = convert_doccano_to_spacy(sys.argv[1])

with open("all_data.json", "w") as file:
    json.dump(training_data, file)