| Feature | Description |
| --- | --- |
| **Name** | `fr_pipeline` |
| **Version** | `0.0.0` |
| **spaCy** | `>=3.7.2,<3.8.0` |
| **Default Pipeline** | `tok2vec`, `ner` |
| **Components** | `tok2vec`, `ner` |
| **Vectors** | 0 keys, 0 unique vectors (0 dimensions) |
| **Sources** | n/a |
| **License** | n/a |
| **Author** | [n/a]() |

### Label Scheme

<details>

<summary>View label scheme (7 labels for 1 components)</summary>

| Component | Labels |
| --- | --- |
| **`ner`** | `CERTIFICATION`, `DATE`, `JOB`, `LIEU`, `ORGANISATION`, `SKILL`, `UNIVERSITY` |

</details>

### Accuracy

| Type | Score |
| --- | --- |
| `ENTS_F` | 98.70 |
| `ENTS_P` | 99.16 |
| `ENTS_R` | 98.24 |
| `TOK2VEC_LOSS` | 128769.03 |
| `NER_LOSS` | 57866.26 |