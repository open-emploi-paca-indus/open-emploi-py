import sys
import json


fo = open(sys.argv[1], "r")

lines = fo.readlines()


for line in lines:
    line =json.loads(line)
    if "labels" in line:
    	line["entities"] = line.pop("label")
    else:
	    line["entities"] = []

    tmp_ents = []
    for e in line["entities"]:
        if e[2] in ['ORGANISATION', 'LIEU', 'DATE', 'SKILL', 'JOB', 'CERTIFICATION', 'UNIVERSITY']:
     	    tmp_ents.append({"start": e[0], "end": e[1], "label": e[2]})
        
        line["entities"] = tmp_ents

    if (len(line["text"]) > 5):
        print (json.dumps({"entities": line["entities"], "text": line["text"]}))