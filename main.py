from fastapi import FastAPI, Depends, HTTPException
import spacy
import os


app = FastAPI()


custom_model_path = os.getenv("CUSTOM_MODEL_PATH")
if not custom_model_path:
    raise EnvironmentError("Please set the CUSTOM_MODEL_PATH environment variable.")

nlp = spacy.load(custom_model_path)



@app.post("/ner")
async def analyze_text(text: str):
    # Perform NER on the input text using your custom model
    doc = nlp(text)
    
    # Extract NER results
    entities = []
    for ent in doc.ents:
        entities.append({
            "text": ent.text,
            "start": ent.start_char,
            "end": ent.end_char,
            "label": ent.label_
        })

    return {"entities": entities}
